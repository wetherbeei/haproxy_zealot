NAME=haproxy-1.4.21
SOURCE=$NAME.tar.gz
if [ ! -f $SOURCE ]
then
  wget http://haproxy.1wt.eu/download/1.4/src/haproxy-1.4.21.tar.gz
fi
tar -xf $SOURCE
cd $NAME
make TARGET=linux26 USE_PCRE=1
make PREFIX=.. install
cd ..
if [ ! -f haproxy.cfg ]
then
  install -D -m644 $NAME/examples/haproxy.cfg haproxy.cfg
fi
rm -rf $NAME
